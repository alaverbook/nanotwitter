module Sinatra
	module MyApp 
		module Sessions
			require './helpers/generateimgsrc'

			def self.registered(app)

				show_register = lambda do
					erb :register
				end

				create_register = lambda do
					@username = params['username']
					@email = params['email']
					@img_src_helper = ImgSrcHelper::Generate.new;

					if User.exists?(username: @username)
						flash[:error] = "Your username was already taken..."
						redirect '/register'
					elsif User.exists?(email: @email)
						flash[:error] = "Your email address was already taken..."
						redirect '/register'	
					else
						user = User.create(:first_name => params['first_name'],
							:last_name	=> params['last_name'],
							:username	=> @username,
							:dob 		=> params['dob'],
							:email		=> @email,
							:bio		=> params['bio'],
							:password 	=> params['password'],
							:img_src 	=> @img_src_helper.generateSrc)
						session[:user_id] = user.id
						redirect '/'
					end
				end


				show_login = lambda do
					erb :login
				end


				create_login = lambda do 
					user = User.find_by({username: params['username']})
					if user != nil
						if (params['password'] == user['password'])
							session[:user_id] = user.id
							redirect '/'
						end
					end
					flash[:error] = "Wrong information.."
					redirect '/login'
				end

				logout = lambda do
					session[:user_id] = nil
					redirect '/'
				end

			app.get '/register', &show_register
			app.post '/register', &create_register
			app.get '/login', &show_login
			app.post '/login', &create_login
			app.get '/logout', &logout

			end

		end
	end
end