module Sinatra
	module MyApp 
		module Tweets

			def self.registered(app)

				create_tweet = lambda do
					content_type :json
					
					@me = User.find_by({id:session[:user_id]});

					if @me == nil 

						@my_tweet = Tweet.create(
							text: params[:tweet_text],
							user_id: 1,
							);

					else
						@my_tweet = @me.tweets.create(
							text: params[:tweet_text],
							user_id: @me.id
							);
					end
	

				#need to be changed since serialized hash now has time_string
				@my_tweet.to_json(:include => :user)
				end

				tweet = lambda do

					if !is_authenticated?
						require_logged_in
					end

					@user = User.find_by({id: session[:user_id]})
					Tweet.create(:text => params['text'],
						:user_id => @user.id);
					redirect back
				end


			app.post '/create_tweet', &create_tweet
			app.post '/tweet', &tweet

			end
			
		end
	end
end