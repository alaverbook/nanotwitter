module Sinatra
	module MyApp 
		module API
			module V1

				def self.registered(app)

					get_recent_tweets = lambda do 

						@tweets = Tweet.order(created_at: :desc).first(10);

						if @tweets
							@tweets.to_json;
						else
							error 404, {:error => "Tweet not found"}.to_json;
						end

					end

					get_tweet_id = lambda do

						@tweets = Tweet.find_by({id:params[:id]});

						if @tweets
							@tweets.to_json;
						else
							error 404, {:error => "Tweet not found"}.to_json;
						end

					end

					get_user_id = lambda do

						@user = User.find_by({id:params[:id]});

						if @user
							@user.to_json;
						else
							error 404, {:error => "User not found"}.to_json;
						end

					end

					get_user_id_tweets = lambda do
						@tweets = Tweet.where({user_id:params[:id]}).order(created_at: :desc).first(10);

						if @tweets
							@tweets.to_json;
						else
							error 404, {:error => "User tweets not found"}.to_json;
						end

					end


					app.get '/api/v1/tweets/recent', &get_recent_tweets
					app.get '/api/v1/tweets/:id', &get_tweet_id
					app.get '/api/v1/users/:id', &get_user_id
					app.get '/api/v1/users/:id/tweets', &get_user_id_tweets


				end
			end

		end
	end
end