  function showModal(param){
    if(param=="tweet"){
     $("#myTweetModal").modal('show');
    }
    else if(param=="login"){
       $("#myLoginModal").modal('show');
    }
  };

  function textCounter(field,field2,maxlimit)
  {
    var countfield = document.getElementById(field2);
    if ( field.value.length > maxlimit ) {
      field.value = field.value.substring( 0, maxlimit );
      return false;
    } 

    else {
      countfield.value = maxlimit - field.value.length;
    }

  }

  function setTweetDrag(){


    $("#myTweetModal").draggable({
      handle: ".modal-header"
    }); 


  }

  function createTweet(id)
  { 
    var text = $("#"+id).val();
    $("#"+id).val("");

    $.ajax({
      type: "POST",
      url: "/create_tweet",
      data: {tweet_text: text},
      success: function(tweet) {
        $("#all_tweet_container").prepend('<li class="list-group-item each_tweet_container"><div>'+
     '<input type="image" src= ' + tweet["user"]["img_src"] + ' class="user_img"></div>'+
    '<span class="tweet_name"><a href="/user/' + tweet["user"]["username"] + '">' + tweet["user"]["first_name"] + ' ' + tweet["user"]["last_name"] + '</a> </span> <span class="tweet_username_time"> @' + tweet["user"]["username"] + '\u2022 ' + tweet["time_string"] + 
    '</span><br>'+
     tweet["text"] +
    '<br>'+
  '</li>');
      document.getElementById('counter').value = 140;
      
      }
    });

  }

    function show_hide(ele){
       var hidden_ele = ele.getElementsByClassName('tweet_more')[0];
       if( hidden_ele.style.display == '' || hidden_ele.style.display == 'none'){
          hidden_ele.style.display = 'block';
        }
        else{
          hidden_ele.style.display = 'none';
        }

    }