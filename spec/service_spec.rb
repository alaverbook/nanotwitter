  require_relative '../app'
  require 'rspec'
  require 'rack/test'
  #require 'rspec'
  #require 'rack/test'
  require 'minitest/spec'
  require 'minitest/autorun'

  #require 'app.rb'


  describe "service" do
    include Rack::Test::Methods

    def app
      Sinatra::Application
    end

    describe "GET on /api/v1/tweets/:id" do
      before(:each) do

       Tweet.delete_all
       User.delete_all

       @user = User.create(
        :first_name => "paul",
        :last_name  => "smith",
        :username => "psmith",
        :dob    => "1990/01/10",
        :email    => "paul@pauldaix.net",
        :bio    => "rubyist",
        :password   => "strongpass",
        :img_src => "/images/lebron.png"
        )

       @tweet = Tweet.create(
        :text => "I had a great day today",
        :user_id => @user.id
        )
     end

     it "should return a valid tweet text" do
      get '/api/v1/tweets/' + @tweet.id.to_s
      last_response.should be_ok
      attributes = JSON.parse(last_response.body)
      attributes["text"].should == "I had a great day today"
    end


  end













  describe "GET on /api/v1/users/:id" do
    before(:each) do

      User.delete_all

      @user = User.create(
        :first_name => "paul",
        :last_name  => "smith",
        :username => "psmith",
        :dob    => "1990/01/10",
        :email    => "paul@pauldaix.net",
        :bio    => "rubyist",
        :password   => "strongpass",
        :img_src => "/images/lebron.png"
        )
    end

    it "should return a user by name" do
      get '/api/v1/users/' + @user.id.to_s
      last_response.should be_ok
      attributes = JSON.parse(last_response.body)
      attributes["first_name"].should == "paul"
    end

    it "should not return a user by last name" do
      get '/api/v1/users/' + @user.id.to_s
      last_response.should be_ok
      attributes = JSON.parse(last_response.body)
      attributes["last_name"].should == "smith"
    end

    it "should not return a user's username" do
      get '/api/v1/users/' + @user.id.to_s
      last_response.should be_ok
      attributes = JSON.parse(last_response.body)
      attributes["username"].should == "psmith"
    end

    it "should return a user with an email" do
      get '/api/v1/users/' + @user.id.to_s
      last_response.should be_ok
      attributes = JSON.parse(last_response.body)
      attributes["email"].should == "paul@pauldaix.net"
    end

    it "should not return a user's password" do
      get '/api/v1/users/' + @user.id.to_s
      last_response.should be_ok
      attributes = JSON.parse(last_response.body)
      attributes.should_not have_key("password")
    end
    it "should return a user with a bio" do
      get '/api/v1/users/' + @user.id.to_s
      last_response.should be_ok
      attributes = JSON.parse(last_response.body)
      attributes["bio"].should == "rubyist"
    end
    it "should return a 404 for a user that doesn't exist" do
      get '/api/v1/users/2'
      last_response.status.should == 404
    end
  end



end



describe User do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  before(:each) do
    Tweet.delete_all
    User.delete_all
  end
  describe "Check valid user model" do
    before(:each) do
        @user = User.create(
      :first_name => "John",
      :last_name  => "Tucker",
      :username => "JTuck1",
      :dob    => "1990/01/10",
      :email    => "paul@pauldix.net",
      :bio    => "rubyist",
      :password   => "strongpass",
      :img_src => "/images/lebron.png",
      )
    end
    it "it should be a valid user" do
      @user.should be_valid
    end
    it "it has a valid first name" do
      @user.first_name.should == ("John")
    end
    it "it has a valid last name" do
      @user.last_name.should == ("Tucker")
    end
  end

  describe "Check invalid user model" do

    it "it has a invalid first name" do
      @user = User.create(
      :first_name => "J",
      :last_name  => "Tucker",
      :username => "JTuck2",
      :dob    => "1990/01/10",
      :email    => "paul@pauldix.net",
      :bio    => "rubyist",
      :password   => "strongpass",
      :img_src => "/images/lebron.png",
      )
      @user.should_not be_valid
    end


  end


end



describe Tweet do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  before(:each) do
    Tweet.delete_all
    User.delete_all

    @user = User.create(
      :first_name => "paul",
      :last_name  => "smith",
      :username => "psmith",
      :dob    => "1990/01/10",
      :email    => "paul@pauldaix.net",
      :bio    => "rubyist",
      :password   => "strongpass",
      :img_src => "/images/lebron.png"
      )


  end 

  describe "Check Tweet Methods" do

    it "tweet 4 seconds difference" do
      @now_sec = Time.now - 4 #seconds

      @tweet = Tweet.create(
        :text => "I had a great day today",
        :user_id => @user.id,
        :created_at => @now_sec)
      @tweet.time_string.should == "Now"

    end

    it "tweet 59 seconds difference" do
      @now_sec = Time.now - 59 #seconds

      @tweet = Tweet.create(
        :text => "I had a great day today",
        :user_id => @user.id,
        :created_at => @now_sec)
      @tweet.time_string.should == "60s" #ceiling function used...

    end

    it "tweet 65 seconds(2min) difference" do
      @now_sec = Time.now - 65 #seconds

      @tweet = Tweet.create(
        :text => "I had a great day today",
        :user_id => @user.id,
        :created_at => @now_sec)
      @tweet.time_string.should == "2m"

    end

    it "tweet 24 hours difference" do
      @now_sec = Time.now - (3600*24) #seconds

      @tweet = Tweet.create(
        :text => "I had a great day today",
        :user_id => @user.id,
        :created_at => @now_sec)
      @tweet.time_string.should == "1d"

    end


  end

  describe "Checking Tweet model" do

    describe "Check valid tweet" do

      it "should be a valid tweet" do
       @tweet = Tweet.create(
        :text => "I had a great day today",
        :user_id => @user.id)
       @tweet.should be_valid
     end


     describe "Creating invalid tweets" do

      it "should have an invalid text length" do  

       @tweet = Tweet.create(
        :text => "I had a great day todayItodayItodayItodayI had a greI had a greI had a greI had a greI had a greI had a greI had a greI had a greI had a gred",
        :user_id => @user.id)

       @tweet.should_not be_valid

     end


   end

 end

 # describe Follower do

 #  describe "Checking valid follower" do
 #    @user = User.create(
 #      :first_name => "josh",
 #      :last_name  => "jefferson",
 #      :username => "jjefferson",
 #      :dob    => "1990/01/10",
 #      :email    => "josh@jefferson.net",
 #      :bio    => "twitter user",
 #      :password   => "password",
 #      :img_src => "/images/lebron.png"
 #      )

    
 #  end

  
 # end

end

end
