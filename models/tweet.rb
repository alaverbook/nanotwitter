class Tweet < ActiveRecord::Base
	#has_many :timelines
	validates :text, length: {maximum: 140}
	#validates :first_name, :last_name, :username, length: {minimum:2, maximum: 10}
	#validates :text,:first_name,:last_name,:username, presence: true #:img_src
	belongs_to :user

	def time_string
		@sec_since = (Time.now.utc - Time.parse(self.created_at.to_s))

		#seconds
		if(@sec_since <= 5)
			"Now"
		elsif(@sec_since<=60)
			@sec_since.ceil.to_s + "s"
		elsif(@sec_since<=3600)
			(@sec_since/60).ceil.to_s + "m"
		elsif(@sec_since < 86400) #within a day
			(@sec_since/3600).ceil.to_s + "h"
		elsif (@sec_since < 172800)
			"1d"
		else 
			self.created_at.strftime("%b %d")
		end
	end
	
	def serializable_hash(options={})
		super options.merge(:methods => :time_string)
	end
end