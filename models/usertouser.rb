class Usertouser < ActiveRecord::Base
	validates :follower_id, presence: true
	validates :followee_id, presence: true

	def self.following(followee_id, follower_id)
		following = Usertouser.where({followee_id: followee_id, follower_id: follower_id});
		if following != nil
			if !following.empty?
				return true
			end
		end
		return false
	end
end