class User < ActiveRecord::Base
	#validates_uniqueness_of :username, :email
	
	validates :password, length: {minimum:4, maximum:15}
	validates :first_name, :last_name, :username, length: {minimum:2, maximum: 25}
 	validates :username, :first_name, :last_name, :email, presence: true

	has_many :tweets
	#has_and_belongs_to_many :users

	def to_json
		super(:except => :password)
	end

	def self.tweets_of_followers(user_id)
		follower_tweets = Array.new
		usertousers = Usertouser.where({follower_id: user_id})
		usertousers.each do |usertouser|
			follower_tweets = follower_tweets + Tweet.where({user_id: usertouser.followee_id})
		end
		follower_tweets = follower_tweets + Tweet.where({user_id: user_id})
		if follower_tweets.size > 1
			follower_tweets.sort! { |a,b| b.created_at <=> a.created_at}
		end
		follower_tweets = follower_tweets.first(100)
		return follower_tweets.to_a
	end
end