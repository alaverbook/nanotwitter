module ImgSrcHelper
	class Generate
		def initialize
			@r = Random.new
		end

		def generateSrc

			case @r.rand(0..8)
			when 0
				@img_src = "/images/leo.jpg"
			when 1
				@img_src = "/images/lebron.png"
			when 2
				@img_src = "/images/ed.jpg"
			when 3
				@img_src = "/images/katy.png"
			when 4
				@img_src = "/images/beyonce.jpg"		
			when 5 
				@img_src = "/images/person.jpg"
			when 6
				@img_src = "/images/person2.jpg"
			when 7
				@img_src = "/images/person3.jpg"
			when 8
				@img_src = "/images/person4.jpg"
			end

			return @img_src

		end



	end

end