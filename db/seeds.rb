require 'faker'
require './helpers/generateimgsrc.rb'
require 'csv'


@my_faker_internet = Faker::Internet
@my_faker_name = Faker::Name
@my_faker_company = Faker::Company
@img_src_helper = ImgSrcHelper::Generate.new;


@initial_time = Time.now

#create the test user
 @temp = User.new(id: 1001,first_name: "tester", last_name: @my_faker_name.last_name,
  username: "test",password: "test",
  email: @my_faker_internet.email,bio: @my_faker_company.catch_phrase,img_src:@img_src_helper.generateSrc,dob:"01/01/2001")
@temp.save!

#create the test_user
 @temp = User.new(id: 1002,first_name: "test", last_name: @my_faker_name.last_name,
  username: "test_user",password: "test",
  email: @my_faker_internet.email,bio: @my_faker_company.catch_phrase,img_src:@img_src_helper.generateSrc,dob:"01/01/2001")
@temp.save!

#ActiveRecord::Base.transaction do
#user follows itself
#Usertouser.create(followee_id:@temp.id, follower_id:@temp.id)

inserts = []

#************************************************************
#sql single mass inserts are faster, 45 sec vs 240sec
 CSV.foreach('db/seed_data/users.csv') do |row|
   inserts.push("('" + (row[0].gsub "'", "''") + "','" + (row[1].gsub "'", "''") + "','" + (@my_faker_name.last_name.gsub "'", "''") + "','" + 
          (@my_faker_internet.user_name.gsub "'", "''") + "','" + "test" + "','" + @my_faker_internet.email + "','" + 
          @my_faker_company.catch_phrase + "','" + @img_src_helper.generateSrc + "','" + "01/01/2001" + "','" + Time.now.to_s + "','" + Time.now.to_s + "')")
 end

sql = "INSERT INTO Users (id, first_name, last_name, username, password, email, bio, img_src, dob, created_at, updated_at) VALUES #{inserts.join(", ")}"

connection = ActiveRecord::Base.connection()
connection.execute(sql)

sql1 = "SELECT setval('users_id_seq', (SELECT max(id) FROM users))"

connection = ActiveRecord::Base.connection()
connection.execute(sql1)

inserts2 = []
  CSV.foreach('db/seed_data/tweets.csv') do |row|
    user = User.find_by({id:row[0]})
   inserts2.push("('" + (row[1].gsub "'", "''") + "','" + (row[0].gsub "'", "''") + "','" + row[2] + "','" + row[2] + "')")
end

 sql2 = "INSERT INTO Tweets (text, user_id,created_at,updated_at) VALUES #{inserts2.join(", ")}"

connection.execute(sql2)


inserts3 =[]
   CSV.foreach('db/seed_data/follows.csv') do |row|
  # Usertouser.create(followee_id:row[0],follower_id:row[1])
    inserts3.push("('" + (row[0].gsub "'", "''") + "','" + (row[1].gsub "'", "''") + "','" + Time.now.to_s + "','" + Time.now.to_s + "')")

    
  end
sql3 = "INSERT INTO Usertousers (followee_id,follower_id,created_at,updated_at) VALUES #{inserts3.join(", ")}"

connection.execute(sql3)

p "Time Taken - " + (Time.now - @initial_time).to_s + " seconds"

p "Seed successful!"