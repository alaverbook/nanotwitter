class CreateUsers < ActiveRecord::Migration
    def self.up
		create_table :users do |t|
			t.string :first_name,:last_name,:username,:password,:email,:bio,:img_src,:dob
			#t.integer :timeline_id
			t.timestamps null: false
		end
	end
	def self.down
		drop_table :users
	end
end
