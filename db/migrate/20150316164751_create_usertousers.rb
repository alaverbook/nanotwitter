class CreateUsertousers < ActiveRecord::Migration
    def self.up
      create_table :usertousers do |t|
        t.integer :followee_id,:follower_id
        t.timestamps null: false
      end

      add_index :usertousers, [:followee_id, :follower_id]
    end

  def self.down
    drop_table :usertousers
  end
end
