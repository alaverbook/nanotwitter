class CreateTweets < ActiveRecord::Migration
  	def self.up
		create_table :tweets do |t|
			t.string :text
			t.integer :user_id #, :timeline_id
			t.timestamps null: false
			t.index :created_at
		end
	end
	def self.down
		drop_table :tweets
	end
end