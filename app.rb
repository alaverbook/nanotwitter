require 'sinatra'
require 'sinatra/activerecord'
require 'sinatra/flash'
#require_relative 'models/user'
#require './models/timeline'
require './models/tweet'
require './models/user'
require './models/usertouser'
require './helpers/helpers'
require './routes/tweets'
require './routes/sessions'
require './routes/apis'
require 'json'
require 'redis'
require 'faker'

enable :sessions, :method_override
set :session_secret, 'This is a secret key'
set :method_override, true

  helpers Sinatra::MyApp::Helpers

  register Sinatra::MyApp::Tweets
  register Sinatra::MyApp::Sessions
  register Sinatra::MyApp::API::V1



configure do
	$redis = Redis.new(:host=>'pub-redis-17295.us-east-1-3.4.ec2.garantiadata.com',:port=>17295,:password=>'test')
end

get '/' do
	if !is_authenticated?
		#@init_time = Time.now
		if $redis.get("top100").nil?
		 	$redis.set("top100",Tweet.order(created_at: :desc).first(100).to_json(:include => {:user => { :only => [:img_src,:username,:first_name,:last_name] }})) 
		 	$redis.expire("top100",1)
		end
		@tweets = JSON.parse($redis.get("top100"))
	else
		#this should display only followed users content
		@tweets = JSON.parse(User.tweets_of_followers(session[:user_id]).to_json(:include => {:user => { :only => [:img_src,:username,:first_name,:last_name] }}))
	end
	# avg 0.24254702 sec, take method have similar times, but faster on testing loader.io
	#@tweets = Tweet.order('created_at DESC').limit(100) #avg 0.00050001516666 sec
	#p (Time.now - @init_time).to_s
	erb :timeline
end

get '/user/profile' do
	if !is_authenticated?
		require_logged_in
	end

	@user = User.find_by({id: session[:user_id]});

	@track = Usertouser.where({follower_id: @user.id});

	@tweets =  @user.tweets.order(created_at: :desc)

	@users = Array.new
	if @track != nil && !@track.empty?
		@track.each do |usertouser|
			@users += User.where({id: usertouser.followee_id})
			@tweets += Tweet.where({user_id: usertouser.followee_id});
		end
		@users.sort! { |a,b| b.username <=> a.username}
		@tweets.sort! { |a,b| b.created_at <=> a.created_at}
	end

	@tweets = JSON.parse(@tweets.to_json(:include => :user))

	erb :profile
end

get '/user/:username' do
	@tweets = Array.new
	@user = User.find_by({username: params[:username]})
	#@tweets = Tweet.where({user_id: @user.id})

	@tweets = JSON.parse(@user.tweets.order(created_at: :desc).to_json(:include => :user))

	erb :user
end

# add username and name searching to results
# maybe change regex for spacing
def searchTweets(search_terms)
	@tweets = Array.new
	count = 0
	search_terms.each do |term|
		if count == 0
			@tweets = Tweet.where("text ILIKE ?", "%#{term}%").all
		else
			@tweets = @tweets & Tweet.where("text ILIKE ?", "%#{term}%").all
		end
		count += 1
	end
	return @tweets.to_a
end

def searchUsers(search_terms)
	@users = Array.new
	count = 0
	search_terms.each do |term|
		if count == 0
			@users = User.where("username ILIKE ?", "%#{term}%").all
		else
			@users = @users & User.where("username ILIKE ?", "%#{term}%").all
		end
		count += 1
	end
	return @users.to_a
end

get '/search' do
	@search_terms = Array.new
	@tweets = Array.new
	@users = Array.new
	if (params['search_values'] != nil)
		@search_terms = params['search_values'].split(" ")
	end
	@tweets = Hash.new
	if @search_terms.empty?
		# this should display the most recent tweets for blank search request
		@tweets = Tweet.order(created_at: :desc).first(100)
		@users = User.order(username: :asc).all
	else 
		# this should select tweets with each keyword
		@tweets = searchTweets(@search_terms)
		@users = searchUsers(@search_terms)
	end
		# if no tweets found given some search term other than empty string
		# do not execute so error will be displayed
	if @tweets != nil && !@tweets.empty?

		if @tweets.size > 1
			@tweets.sort! { |a,b| b.created_at <=> a.created_at}
		end

		@tweets = @tweets.first(100)

		@tweets = JSON.parse(@tweets.to_json(:include => :user))

	end
	if @users != nil && !@users.empty? && !@search_terms.empty?
		if @users.size > 1
			@users.sort! { |a,b| b.username <=> a.username}
		end
		@users = JSON.parse(@users.to_json)
	end

	erb :search
end

post '/follow' do
	if !is_authenticated?
		require_logged_in
	end
	p params[:user_getting_followed_id]
	@usertouser = Usertouser.create(followee_id: params[:user_getting_followed_id],
				  					follower_id: session[:user_id]);
	@usertouser.to_json;
	redirect back
end

delete '/follow' do
	if !is_authenticated?
		require_logged_in
	end
	row_to_delete = Usertouser.find_by(followee_id: params['user_getting_followed_id'],
				  									follower_id: session[:user_id]);
	if row_to_delete
		row_to_delete.destroy
       	#row_to_delete.to_json
       	redirect back
    else
		error 404, {:error => "user following user not found"}.to_json
	end
end

get '/loaderio-18ae262f6c268969aef85968a0463acf/' do
	"loaderio-18ae262f6c268969aef85968a0463acf"
end

get '/loaderio-1404c94acdfb5afc45c32df20d1deaf8/' do
  "loaderio-1404c94acdfb5afc45c32df20d1deaf8"
end

#code pito sent in email on 4/13/2015
get '/loaderio-00a472a26f95d5b43f50740ad86bbb1e/' do
	"loaderio-00a472a26f95d5b43f50740ad86bbb1e"
end

get '/test_tweet' do
	@faker_company = Faker::Company
	@test_user = User.find_by({username:"test_user"})
	@user_tweet = @test_user.tweets.create(
							text: @faker_company.bs,
							user_id: @test_user.id
							);
	"tweet created"
end

get '/test_follow' do
	#content_type :json
	@test_user = User.find_by({username:"test_user"})
	@rand_user = User.offset(rand(User.count)).first
	if(Usertouser.find_by(followee_id: @rand_user.id,
				  					follower_id: @test_user.id).nil?)
	Usertouser.create(followee_id: @rand_user.id,
				  					follower_id: @test_user.id)
	"followed"
	else
	Usertouser.find_by(followee_id: @rand_user,
				  					follower_id: @test_user).destroy
	"unfollowed"
	end
end

get '/reset' do
	User.find_by({username:"test_user"}).tweets.delete_all
	Usertouser.where({follower_id:@test_user}).delete_all
	"resetted"
end